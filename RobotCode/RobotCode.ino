#include <Servo.h> 
#include <Oscillator.h>
#include <US.h>
#include <Otto.h>
#include <SoftwareSerial.h>


#define PIN_LEFT_LEG   2   
#define PIN_RIGHT_LEG  3  
#define PIN_LEFT_FOOT  4  
#define PIN_RIGHT_FOOT 5 

SoftwareSerial BT(6,7); // TX RX
String state;
Otto Otto;
int i;
int T=1000;              //Initial duration of movement
int moveId=0;            //Number of movement
int moveSize=15;         //Asociated with the height of some movements
bool status = false;

void setup() {
  Serial.begin(9600);
  Otto.init(PIN_LEFT_LEG,PIN_RIGHT_LEG,PIN_LEFT_FOOT,PIN_RIGHT_FOOT,true);
  
  BT.begin(9600);
  Serial.println("Robot Start");
  
}

void loop() {
  while(BT.available()){
    delay(10);
    char c = BT.read();
    state += c;
  }
  if(state.length() > 0){
    Serial.println(state);
      if(state == "left") {
        //leftMove();
      }

      else if(state == "right") {
        //rightMove();
      }

      else if(state == "Forward") {
        for(i=0;i<2;i++){
          Serial.println(i);
          Otto.walk(2,500,-1);
          delay(50);
        }
      }

      else if(state == "Backward")
      {
        for(i=0;i<2;i++){
          Serial.println(i);
          Otto.walk(2,500,1);
          delay(50);
       }
      }
     
       else
      {
        Serial.println("Another Command");
      }
      
  state ="";
  } 

}
